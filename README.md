<!-- vale off -->
# jfxs / robot-framework
<!-- vale on -->

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=for-the-badge)](LICENSE)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/op_so/docker/robot-framework?style=for-the-badge)](https://gitlab.com/op_so/docker/robot-framework/pipelines)
[![Robot Framework Tests](https://op_so.gitlab.io/docker/robot-framework/all_tests.svg)](https://op_so.gitlab.io/docker/robot-framework/report.html)

A lightweight automatically updated and tested `multiarch` amd64 and arm64 Docker image to run [Robot Framework](https://robotframework.org/) tests and containing:

* **lightweight** image based on Alpine Linux only 40 MB,
* `multiarch` with support of **amd64** and **arm64**,
* **non-root** container user,
* **automatically** updated by comparing the software bill of materials (`SBOM`) changes,
* image **signed** with [Cosign](https://github.com/sigstore/cosign),
* an **software bill of materials (`SBOM`) attestation** added using [`Syft`](https://github.com/anchore/syft),
* available on **Docker Hub** and **Quay.io**.

[![GitLab](https://shields.io/badge/Gitlab-554488?logo=gitlab&style=for-the-badge)](https://gitlab.com/op_so/docker/robot-framework) The main repository.

[![Docker Hub](https://shields.io/badge/dockerhub-1D63ED?logo=docker&logoColor=white&style=for-the-badge)](https://hub.docker.com/r/jfxs/robot-framework) The Docker Hub registry.

[![Quay.io](https://shields.io/badge/quay.io-E5141F?logo=redhat&logoColor=white&style=for-the-badge)](https://quay.io/repository/ifxs/robot-framework) The Quay.io registry.

This image has:

* [Robot Framework](https://github.com/robotframework/robotframework)
* [Robot Framework SeleniumLibrary](https://github.com/robotframework/SeleniumLibrary)
* [Robot Framework Requests](https://github.com/bulkan/robotframework-requests)
* [`Robotidy`](https://github.com/MarketSquare/robotframework-tidy)
* [`Robocop`](https://github.com/MarketSquare/robotframework-robocop)
* [Task](https://taskfile.dev/)
* A task **mask** to hide sensitive data in the Robot Framework reports.
* A task **badge** to generate a Robot Framework badge in SVG.

<!-- vale off -->
## Running Robot Framework
<!-- vale on -->

Example to run Robot Framework tests in your current directory:

```shell
docker run -t --rm -v $(pwd):/tests jfxs/robot-framework robot --outputdir /tests /tests/tests/RF
```

Example to add a Robot Framework library and run Robot Framework tests in your current directory:

```shell
docker run -t --rm -v $(pwd):/tests jfxs/robot-framework /bin/sh -c "pip install --user robotframework-faker && robot --outputdir /tests /tests/tests/RF"
```

## [Task](https://taskfile.dev/) robot.yml template

This image includes a [Task](https://taskfile.dev/) Robot Framework template with some useful tasks:

* badge: create a badge all_tests.svg from the results of the Robot Framework tests. ![Robot tests badge](https://gitlab.com/op_so/docker/robot-framework/-/raw/master/all_tests.svg "Robot tests badge")
If all tests pass, the background is `green`. If at least one test fails, the background is `red`. If at least a test has a skip status and all other pass, the background is `orange`.

```shell
      Usage: task robot:badge FILE|F=<output_file_path> [DIR|D=<badge_directory>] [IMG|I=<docker_image:tag>] [PULL|P=<n|N>]

      Arguments:
        FILE | F  Output XML results file (required)
        DIR  | D  Generated badge directory (optional, current directory by default)
        IMG  | I  Docker Robot Framework image to use (optional, by default {{.IMAGE_ROBOT}})
        PULL | P  Pull docker image (optional, by default yes)

      Requirements:
        - pybadges lib or docker
```

```shell
# Example:
docker run -t --rm -v $(pwd):/tests jfxs/robot-framework /bin/sh -c "task --taskfile /robot.yml badge FILE=/tests/reports/output.xml"
```

* cop: static code analysis of Robot Framework language with [`Robocop`](https://github.com/MarketSquare/robotframework-robocop).

```shell
      Usage: task robot:cop DIR|D=<tests_dir> [ARG|A="cli_arguments"] [IMG|I=<docker_image:tag>] [PULL|P=<n|N>]

      Arguments:
        DIR  | D  Directory or file to format (required)
        ARG  | A  Arguments and options (optional)
        IMG  | I  Docker Robot Framework image to use (optional, by default {{.IMAGE_ROBOT}})
        PULL | P  Pull docker image (optional, by default yes)

      Requirements:
        - robocop or docker
```

```shell
# Example:
docker run -t --rm -v $(pwd):/tests jfxs/robot-framework /bin/sh -c "task --taskfile /robot.yml cop DIR=/tests/tests/RF"
```

* mask: mask sensitive data of a file, especially in output.xml Robot Framework file before publishing.

```shell
      Usage: task robot:mask FILE|F=<file_path> DATA|D=paswword1,password2

      Arguments:
        FILE | F  Fix files (required)
        DATA | D  Data to mask separated by comma (required)
```

```shell
# Example:
docker run -t --rm -v $(pwd):/tests jfxs/robot-framework /bin/sh -c "task --taskfile /robot.yml mask FILE=/tests/reports/output.xml DATA=paswword1,password2"
```

* tidy: Format Robot Framework files with [`Robotidy`](https://github.com/MarketSquare/robotframework-tidy).

```shell
      Usage: task robot:tidy DIR|D=<tests_dir> [ARG|A="cli_arguments"] [IMG|I=<docker_image:tag>] [PULL|P=<n|N>]

      Arguments:
        DIR  | D  Directory or file to format (required)
        ARG  | A  Arguments and options (optional)
        IMG  | I  Docker Robot Framework image to use (optional, by default {{.IMAGE_ROBOT}})
        PULL | P  Pull docker image (optional, by default yes)

      Requirements:
        - robotidy or docker
```

```shell
# Example:
docker run -t --rm -v $(pwd):/tests jfxs/robot-framework /bin/sh -c "task --taskfile /robot.yml tidy DIR=/tests/tests/RF ARG"--check""
```

You can also install [Task](https://taskfile.dev/) and download the robot.yml template locally. Documentation: [Task templates](https://gitlab.com/op_so/task/task-templates)

## Built with

Docker latest tag is [--VERSION--](https://gitlab.com/op_so/docker/robot-framework/-/blob/master/Dockerfile) and has:
<!-- vale off -->

--SBOM-TABLE--

<!-- vale on -->
Details are available on [`Dockerhub` Overview page](https://hub.docker.com/r/jfxs/robot-framework).

## Versioning

<!-- vale off -->
The Docker tag is defined by the Robot Framework version used and an increment to differentiate build with the same Robot Framework version:
<!-- vale on -->

```text
<robot_version>-<increment>
```

Example: `6.0.2-001`

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available online: <https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub>.

To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the `SBOM` attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | fromjson | .predicate'
```

## Authors

<!-- vale off -->
* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)
<!-- vale on -->

## License

<!-- vale off -->
This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
